import { createStore } from 'redux';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import { isPanelOpen } from './components/reducers';

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2,
} 

const persistedReducers = persistReducer(persistConfig, isPanelOpen);

export const configureStore = () => createStore(persistedReducers);