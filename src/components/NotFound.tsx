import { FC } from "react";

interface NotFoundProps {
    
}
 
const NotFound: FC<NotFoundProps> = () => {
    return ( 
        <div className="notfound-container">
            <div></div>
            <div>
                <p className="notfound-text">OOPS!</p>
                <p className="sth-wrong">Something went wrong.</p>
            </div>
        </div>
     );
}
 
export default NotFound;