import { FC } from "react";
import NavIcon from '../imgs/navbar-icon.svg';
import 'react-sliding-side-panel/lib/index.css';
import NavItems from "./NavItems";
import { useDispatch, useSelector } from "react-redux";
import { togglePanel } from "./reducers";
import { RootState } from "./types/RootStateInterface";


interface NavbarProps {
    
}

const Navbar: FC<NavbarProps> = () => {

    const dispatch = useDispatch();
    const isSidePanelOpen = useSelector((state: RootState) => state.isPanelOpen);

    return ( 
        <div className="navbar">
            <div className="navbar_left-half">
                <img src={`${NavIcon}`} alt="star icon" className="nav-icon" onClick={() => localStorage.clear()}/>
                <div className="line"></div>
            </div>
            <div className="navbar_right-half">
                <NavItems isSidebar={false} />
            </div>
            {!isSidePanelOpen &&
                <div className="hamburger-container">
                    <img 
                        src='../Images/icon-hamburger.svg' 
                        alt='hamburger' 
                        onClick={() => dispatch(togglePanel())}
                        className="hamburger-icon"
                        height="100%"
                    />
                </div>
            }
        </div>
     );
}
 
export default Navbar;