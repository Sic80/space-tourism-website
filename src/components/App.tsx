import '../scss/style.css';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import Destinations from './Destinations';
import Crew from './Crew';
import Technology from './Technology';
import NotFound from './NotFound';
import { useEffect } from 'react';
import SideBar from './SideBar';
import { fetchData } from './fetchData';

function App() {

	useEffect(() => {
		fetchData();
	});

	return (
		<div className="app">
			<div className="sidebar">
				<SideBar />
			</div>
			<Switch>
				<Route exact path='/' component={Home} />  
				<Route exact path="/destinations/:exp(exp)?"  component={Destinations} />  
				<Route exact path='/crew/:exp(exp)?'  component={Crew} />  
				<Route exact path='/technology/:exp(exp)?'  component={Technology} />  
				<Route component={NotFound} />
			</Switch>
		</div>
	);
}

export default App;