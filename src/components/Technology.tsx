import { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { fetchData } from "./fetchData";
import Loading from "./Loading";
import Navbar from "./Navbar";
import { TechnologyData } from "./types/TechnologyDataInterface";

const Technology: FC = () => {

    const { pathname } = useLocation();
    const splittedPath = pathname.split('/');
    const dataJson = localStorage.getItem("data");
    const parsedData = dataJson ? JSON.parse(dataJson) : {};
    const [data, setData] = useState(parsedData);
    const technologyData = data ? data.technology : null;
    const [currentTechnology, setCurrentTechnology] = useState<TechnologyData>(technologyData ? technologyData[0] : null);
    const [clickedNavButton, setClickedNavButton] = useState<number>(0);

    useEffect(() => {
        if(!currentTechnology) {
            fetchData();
            const dataJson = localStorage.getItem("data");
            const parsedData = dataJson ? JSON.parse(dataJson) : {};
            setData(parsedData);
            if (technologyData) {
                setCurrentTechnology(technologyData[0]);
            }
        }      
    }, [currentTechnology, technologyData, data]);

    useEffect(() => {
        if (splittedPath[splittedPath.length - 1] === "exp" && technologyData) {
            const randomIdx = Math.floor(Math.random() * 3);
            setCurrentTechnology(technologyData[randomIdx]);
            setClickedNavButton(randomIdx);
        }
    }, []); // eslint-disable-line

    return (
        currentTechnology ? (
            <div className="technology-container">
                <Navbar />
                <div className="technology-header">
                    <span className="technology-h-number">
                        03
                    </span>
                    SPACE LAUNCH 101
                </div>
                <div className="tab-cell-img-container">
                    <img className="tab-cell-img" src={`../${currentTechnology.img.tablet}`} alt={currentTechnology.name} />
                </div>
                <div className="technology-info-grid">
                    <div className="technology-data">
                        <div className="nav-col">
                            <div className="bullet-container">
                                {technologyData.map((technology: TechnologyData, techIdx: number) =>
                                    <div 
                                        key={techIdx} 
                                        className={`nav-bullet ${clickedNavButton === techIdx && 'clicked-bullet'}`}
                                        onClick={() => {
                                            setCurrentTechnology(technology);
                                            setClickedNavButton(techIdx);
                                        }}
                                    >
                                        {techIdx + 1}
                                    </div>
                                )}

                            </div>
                        </div>
                        <div className="data-col">
                            <p className="mini-header">THE TERMINOLOGY...</p>
                            <p className="technology-name">{currentTechnology.name}</p>
                            <p className="technology-description">{currentTechnology.description}</p>
                        </div>
                    </div>
                    <div className="technology-img-col">
                        <img className="tech-img" src={`../${currentTechnology.img.desktop}`} alt={currentTechnology.name} />
                    </div>
                </div>
            </div>
        ) : (
            <Loading />
        )
    );
}
 
export default Technology;