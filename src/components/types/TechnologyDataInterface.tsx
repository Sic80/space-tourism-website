export interface TechnologyData {
    name: string;
    description: string;
    img: {
            desktop: string;
            tablet: string;
        };
}