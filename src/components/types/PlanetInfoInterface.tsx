export interface PlanetInfo {
    img: string;
    header: string;
    description: string;
    distance: string;
    travelTime: string;
}