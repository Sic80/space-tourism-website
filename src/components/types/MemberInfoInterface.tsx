export interface Member {
    role: string;
    name: string;
    bio: string;
    img: string;
}