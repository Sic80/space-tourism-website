
export const fetchData = () => {
        fetch('/data.json')
        .then(response => {
        	if (response.ok) {
        		return response.json();
        	} else {
            	throw new Error('Something went wrong');
        	}
		})
        .then(result => {
            const data = result;
            localStorage.setItem("data", JSON.stringify(data));
        })
		.catch((error) => {
			console.log(error)
		});
}