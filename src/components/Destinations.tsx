import { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { fetchData } from "./fetchData";
import Loading from "./Loading";
import Navbar from "./Navbar";
import { PlanetInfo } from "./types/PlanetInfoInterface";

 
const Destinations: FC = () => {
    
    const { pathname } = useLocation();
    const splittedPath = pathname.split('/');
    const dataJson = localStorage.getItem("data");
    const parsedData = dataJson ? JSON.parse(dataJson) : null;
    const [data, setData] = useState(parsedData);
    const planetsInfo = data ? data.planets : null ;
    const [currentPlanet, setCurrentPlanet] = useState<PlanetInfo>(planetsInfo ? planetsInfo[0] : null);
    const [clickedPlanet, setClickedPlanet] = useState<number>(0);

    useEffect(() => {
        if(!currentPlanet) {
            fetchData();
            const dataJson = localStorage.getItem("data");
            const parsedData = dataJson ? JSON.parse(dataJson) : {};
            setData(parsedData);
            if (planetsInfo) {
                setCurrentPlanet(planetsInfo[0])
            }
        }  
    }, [currentPlanet, planetsInfo, data]);

    useEffect(() => {
        if (splittedPath[splittedPath.length - 1] === "exp" && planetsInfo) {
            const randomIdx = Math.floor(Math.random() * 4);
            setCurrentPlanet(planetsInfo[randomIdx]);
            setClickedPlanet(randomIdx);
        }
    }, []); // eslint-disable-line

    
    return ( 
        currentPlanet ? (
            <div className="destinations-container">
                <Navbar />
                <div className="destinations-header">
                    <p><span className="header-number">01 </span>PICK YOUR DESTINATION</p>
                </div>
                <div className="planet-grid">
                <div className="planet-img-col">
                    <div className="planet-img" style={{backgroundImage: `url(../${currentPlanet.img})`, backgroundSize:"cover"}}></div> 
                </div>
                    <div className="planet-info-col">
                        <div className="planet-info">
                            <div className="mini-nav-bar">
                                <ul>
                                    {planetsInfo.map((planet: PlanetInfo, planetIdx: number) =>
                                        <li 
                                            key={planetIdx} 
                                            onClick={() => {
                                                setCurrentPlanet(planet);
                                                setClickedPlanet(planetIdx);
                                            }}
                                            className={`nav-items ${planetIdx === clickedPlanet && 'nav-item-selected'}`}
                                        >
                                            {planet.header}
                                        </li>            
                                    )}
                                </ul>
                            </div>
                            <div className="planet-header">
                                {currentPlanet.header}
                            </div>
                            <div className="planet-description">
                                {currentPlanet.description}
                            </div>
                            <div className="planet-additional-info">
                                <div className="distance">
                                    <p className="additional-info-header distance-h">AVG. DISTANCE</p>
                                    <p className="additional-info-data distance-data">{currentPlanet.distance}</p>
                                </div>
                                <div className="traveltime">
                                    <p className="additional-info-header travel-h">EST. TRAVEL TIME</p>
                                    <p className="additional-info-data travel-data">{currentPlanet.travelTime}</p>
                                </div>
                            </div>
    
                        </div>
                    
                    </div> 
                </div>
            </div>
        ) : (
            <Loading />
        )
    );
}
 
export default Destinations;