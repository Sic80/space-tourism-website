import { FC } from "react";
import { useHistory } from "react-router-dom";
import Navbar from "./Navbar";

interface HomeProps {
    
}
 
const Home: FC<HomeProps> = () => {

    const history = useHistory();
    const mainButtonHandler = () => {
        const getRandomUrl = () => {
            const url = ['/destinations', '/crew', '/technology'];
            const randomIdx = Math.floor(Math.random() * 3);
            return url[randomIdx];
        }
        history.push(`${getRandomUrl()}/exp`);
    }

    return ( 
        <div className="home-container">
            <Navbar /> 
            <div className="home-grid">
                <div className="home_jumbo">
                    <p className="jumbo_premise">SO, YOU WANT TO TRAVEL TO</p>
                    <p className="jumbo_capital">SPACE</p>
                    <p className="jumbo_text">
                        <span>Let's face it; if you want to go to space, you might as well </span>
                        <span>genuinely go to outer space and not hover kind of on the </span> 
                        <span>edge of it. Well sit back, and relax because we'll give you a </span>
                        <span>truly out of this world experience!</span>
                    </p>
                </div> 
                <div 
                    className="main-button"
                    onClick={() => mainButtonHandler()}
                >
                    <p className="button_text">EXPLORE</p>
                </div>         
                
            </div> 
        </div>
     );
}
 
export default Home;