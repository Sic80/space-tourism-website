import { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { fetchData } from "./fetchData";
import Loading from "./Loading";
import Navbar from "./Navbar";
import { Member } from "./types/MemberInfoInterface";

const Crew: FC = () => {

    const { pathname } = useLocation();
    const splittedPath = pathname.split('/');
    const dataJson = localStorage.getItem("data");
    const parsedData = dataJson ? JSON.parse(dataJson) : {};
    const [data, setData] = useState(parsedData);
    const crewData = data ? data.crew: null;
    const [currentMember, setCurrentMember] = useState<Member>(crewData ? crewData[0] : null);
    const [clickedBullet, setClickedBullet] = useState<number>(0);

    useEffect(() => {
        if(!currentMember) {
            fetchData();
            const dataJson = localStorage.getItem("data");
            const parsedData = dataJson ? JSON.parse(dataJson) : {};
            setData(parsedData);
            if (crewData) {
                setCurrentMember(crewData[0]);
            }
        }      
    }, [currentMember, crewData, data]);

    useEffect(() => {
        if (splittedPath[splittedPath.length - 1] === "exp" && crewData) {
            const randomIdx = Math.floor(Math.random() * 4);
            setCurrentMember(crewData[randomIdx]);
            setClickedBullet(randomIdx);
        }
    }, []); // eslint-disable-line

    return ( 
        currentMember ? (

            <div className="crew-container">
                <Navbar />
                <div className="crew-grid">
                    <div className="col-left">
                        <div className="crew-header">
                            <p>
                                <span className="crew-number">02</span>
                                MEET YOUR CREW
                            </p>
                        </div>
                        <div className="member-info">
                            <div className="member-role">
                                {currentMember.role}
                            </div>
                            <div className="member-name">
                                {currentMember.name}                       
                            </div>
                            <div className={`member-bio bio-${currentMember.name.substring(0, 3).toLowerCase()}`}>
                                {currentMember.bio}
                            </div>
                            <div className="member-navbar">
                                <ul>
                                    {crewData.map((member: Member, memberIdx: number) => 
                                        <li 
                                            key={memberIdx} 
                                            className={`custom-bullet ${memberIdx === clickedBullet && 'member-selected'}`}
                                            onClick={() => {
                                                setCurrentMember(member); 
                                                setClickedBullet(memberIdx);
                                            }}
                                        >
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col-right">
                        <div className={`img-container-${currentMember.name.substring(0, 3).toLowerCase()}`}>
                            <img className="crew-img" src={`../${currentMember.img}`} alt="currentMember.name"/>
                        </div>
                    </div>
                </div>
            </div>
        ) : (
            <Loading />
        )
     );
}
 
export default Crew;