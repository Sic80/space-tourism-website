import { FC } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { togglePanel } from "./reducers";
import { Page } from "./types/PageInterface";

interface NavItemsProps {
    isSidebar: boolean;
}
 
const NavItems: FC<NavItemsProps> = ({ isSidebar }) => {

    const dispatch = useDispatch();
    const { pathname } = useLocation();
    const history = useHistory();
    const pages: Page[] = [{name: "HOME", url: "/"}, 
                           {name: "DESTINATIONS", url: "/destinations"}, 
                           {name: "CREW", url: "/crew"}, 
                           {name: "TECHNOLOGY", url: "/technology"}];

    return (
        <div className="nav-items">
            <ul>
                {pages.map((page: Page, pageIdx: number) =>
                    <li 
                        key={pageIdx}
                        className={`nav-item ${(pathname === page.url || pathname === `${page.url}/exp`) && 'nav-item-selected'}`}
                        onClick={() => {
                            history.push(page.url);
                            if (isSidebar) {
                                dispatch(togglePanel());
                            }                       
                        }}
                    >
                        <span className="list_numbers">0{pageIdx}</span>
                        <span>{page.name}</span>
                    </li>
                )}
            </ul>
        </div>
      );
}
 
export default NavItems;