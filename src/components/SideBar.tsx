import { FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import SlidingPanel from "react-sliding-side-panel";
import NavItems from "./NavItems";
import { togglePanel } from "./reducers";
import { RootState } from "./types/RootStateInterface";

interface SideBarProps {
    
}
 
const SideBar: FC<SideBarProps> = () => {

    const isSidePanelOpen = useSelector((state: RootState) => state.isPanelOpen);
    const dispatch = useDispatch();

    return (
        <SlidingPanel
                type={'right'}
                size={67.733}
                isOpen={isSidePanelOpen}
                noBackdrop={true}
                panelClassName="side-panel"
                panelContainerClassName="panel-container"
            >
                <div className="panel-content">
                        <div className="x-container">
                            <img 
                                src='../Images/icon-close.svg' 
                                alt='close' 
                                onClick={() => dispatch(togglePanel())}
                                className="close-icon"
                            />
                        </div> 
                        <NavItems isSidebar={true} />
                </div>
            </SlidingPanel>
      );
}
 
export default SideBar;
                            