
interface togglePanelAction {
    type: typeof TOGGLE_PANEL;
}
const TOGGLE_PANEL = "TOGGLE_PANEL";
export const togglePanel = () => ({
    type: TOGGLE_PANEL,
});

export interface IsPanelOpen {
    isPanelOpen: boolean;
}

export const isPanelOpen = (state: IsPanelOpen ={isPanelOpen: false}, action: togglePanelAction) => {
    switch(action.type) {
        case TOGGLE_PANEL: {
            return {isPanelOpen: !state.isPanelOpen};
        }
        default: {
            return state;
        }
    }
}